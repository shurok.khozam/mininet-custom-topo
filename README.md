# Mininet Custom TOPO

## Mininet topology (with s1 or r1)
![Mininet Topology](Mininet-topology-with-s1-or-r0.png)


## IpMininet topology (with s1 or r1)
![Mininet Topology](IpMininet-topology-with-s1-or-r1.png)