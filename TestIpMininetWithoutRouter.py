from ipmininet.ipswitch import IPSwitch
from mininet.log import lg

import ipmininet
from ipmininet.cli import IPCLI
from ipmininet.ipnet import IPNet
from ipmininet.iptopo import IPTopo
from mininet.node import Controller

"""This file contains a simple network topology"""


class SimpleTopo(IPTopo):

    def build(self, *args, **kwargs):

        s1 = self.addSwitch("s1", lo_addresses=['10.1.0.1/24'])
        s2 = self.addSwitch("s2", lo_addresses=['10.2.0.1/24'])
        s3 = self.addSwitch("s3", lo_addresses=['10.3.0.1/24'])
        s4 = self.addSwitch("s4", lo_addresses=['10.4.0.1/24'])

        h3 = self.addHost("h3", defaultRoute='via 10.0.2.3')
        h4 = self.addHost("h4", defaultRoute='via 10.0.2.4')
        h5 = self.addHost("h5", defaultRoute='via 10.0.3.3')
        h6 = self.addHost("h6", defaultRoute='via 10.0.3.4')

        s4s1 = self.addLink(s4, s1)
        s4s1[s1].addParams(ip=("10.0.1.1/24"))
        s4s1[s4].addParams(ip=("10.0.1.2/24"))

        s2s1 = self.addLink(s2, s1)
        s2s1[s1].addParams(ip=("10.0.1.3/24"))
        s2s1[s2].addParams(ip=("10.0.1.4/24"))

        s3s1 = self.addLink(s3, s1)
        s3s1[s1].addParams(ip=("10.0.1.5/24"))
        s3s1[s3].addParams(ip=("10.0.1.6/24"))

        h3s2 = self.addLink(h3, s2)
        h3s2[s2].addParams(ip=("10.0.2.3/24"))
        h3s2[h3].addParams(ip=("10.0.2.5/24"))

        h4s2 = self.addLink(h4, s2)
        h4s2[s2].addParams(ip=("10.0.2.4/24"))
        h4s2[h4].addParams(ip=("10.0.2.6/24"))

        h5s3 = self.addLink(h5, s3)
        h5s3[s3].addParams(ip=("10.0.3.3/24"))
        h5s3[h5].addParams(ip=("10.0.3.5/24"))

        h6s3 = self.addLink(h6, s3)
        h6s3[s3].addParams(ip=("10.0.3.4/24"))
        h6s3[h6].addParams(ip=("10.0.3.6/24"))

        self.addSubnet(nodes=[s1, s2, s3, s4], subnets=["10.0.1.0/24"])
        self.addSubnet(nodes=[s2, h3, h4], subnets=["10.0.2.0/24"])
        self.addSubnet(nodes=[s3, h5, h6], subnets=["10.0.3.0/24"])

ipmininet.DEBUG_FLAG = True
lg.setLogLevel("info")

# Start network
net = IPNet(topo=SimpleTopo(), use_v4=True, allocate_IPs=False, controller=Controller, switch=IPSwitch, waitConnected=True)
try:
    net.start()

    lg.info(net["s1"].cmd("ip -4 route add 10.0.2.0/24 via 10.0.1.4"))
    lg.info(net["s1"].cmd("ip -4 route add 10.0.3.0/24 via 10.0.1.6"))
    lg.info(net["s2"].cmd("ip -4 route add 10.0.3.0/24 via 10.0.1.3"))
    lg.info(net["s3"].cmd("ip -4 route add 10.0.2.0/24 via 10.0.1.5"))

    IPCLI(net)
finally:
    net.stop()
