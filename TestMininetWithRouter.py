from mininet.net import Mininet
from mininet.node import Controller, OVSSwitch
from mininet.topo import Topo
from mininet.cli import CLI
from mininet.node import Node
from mininet.log import setLogLevel, info

class LinuxRouter( Node ):
    "A Node with IP forwarding enabled."
    def config( self, **params ):
        super( LinuxRouter, self).config( **params )
        # Enable forwarding on the router
        self.cmd( 'sysctl net.ipv4.ip_forward=1' )

    def terminate( self ):
        self.cmd( 'sysctl net.ipv4.ip_forward=0' )
        super( LinuxRouter, self ).terminate()

class NetworkTopo( Topo ):

    def build( self, **_opts ):

        info( "*** Creating switches\n" )
        s1 = self.addSwitch( 's1')
        s2 = self.addSwitch( 's2')
        s3 = self.addSwitch( 's3')

        info("*** Creating Routers\n")
        defaultIP = '10.0.1.1/24'
        r0 = self.addNode('r0', cls=LinuxRouter, ip=defaultIP, mac="00:01:00:00:00:00")

        info( "*** Creating hosts\n" )

        h3 = self.addHost('h3', ip="10.0.2.3/24", mac="00:00:00:00:02:03", defaultRoute='via 10.0.2.1')
        h4 = self.addHost('h4', ip="10.0.2.4/24", mac="00:00:00:00:02:04", defaultRoute='via 10.0.2.1')
        h5 = self.addHost('h5', ip="10.0.3.3/24", mac="00:00:00:00:03:03", defaultRoute='via 10.0.3.1')
        h6 = self.addHost('h6', ip="10.0.3.4/24", mac="00:00:00:00:03:04", defaultRoute='via 10.0.3.1')

        hosts1 = [ h3, h4 ]
        hosts2 = [ h5, h6 ]

        info( "*** Creating links\n" )
        self.addLink( s3, r0, intfName2='r0-eth0', params2={ 'ip' : defaultIP })

        self.addLink( s1, r0, intfName2='r0-eth1', params2={ 'ip' : '10.0.2.1/24' })
        self.addLink( s2, r0, intfName2='r0-eth2', params2={ 'ip' : '10.0.3.1/24' })

        for h in hosts1:
            self.addLink(h, s1)
        for h in hosts2:
            self.addLink(h, s2)

def run():
    topo = NetworkTopo()
    net = Mininet(topo=topo, controller=Controller, switch=OVSSwitch, waitConnected=True)

    net.start()

    info( '*** Routing Table on Router:\n' )
    info( net[ 'r0' ].cmd( 'route' ) )

    info( "*** Testing network\n" )
    net.pingAll()

    net.linksBetween('h3','s1')

    info( "*** Running CLI\n" )
    CLI( net )

    info( "*** Stopping network\n" )
    net.stop()


if __name__ == '__main__':
    setLogLevel( 'info' )  # for CLI output
    run()