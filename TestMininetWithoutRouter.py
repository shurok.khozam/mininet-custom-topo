from mininet.net import Mininet
from mininet.node import Controller, OVSSwitch
from mininet.topo import Topo
from mininet.cli import CLI
from mininet.node import Node
from mininet.log import setLogLevel, info

class NetworkTopo( Topo ):

    def build( self, **_opts ):

        info( "*** Creating switches\n" )
        s1 = self.addSwitch('s1')
        s2 = self.addSwitch('s2')
        s3 = self.addSwitch('s3')
        s4 = self.addSwitch('s4')


        info("*** Creating Routers\n")

        info( "*** Creating hosts\n" )

        h3 = self.addHost('h3', ip="10.0.2.5/24", mac="00:00:00:00:02:05", defaultRoute='via 10.0.2.1')
        h4 = self.addHost('h4', ip="10.0.2.6/24", mac="00:00:00:00:02:06", defaultRoute='via 10.0.2.1')
        h5 = self.addHost('h5', ip="10.0.3.5/24", mac="00:00:00:00:03:05", defaultRoute='via 10.0.3.1')
        h6 = self.addHost('h6', ip="10.0.3.6/24", mac="00:00:00:00:03:06", defaultRoute='via 10.0.3.1')


        hosts2 = [h3, h4]
        hosts3 = [h5, h6]

        info( "*** Creating links\n")
        self.addLink(s4, s1, intfName2='s1-eth0', params2={'ip': '10.0.1.1/24'})
        self.addLink(s2, s1, intfName2='s1-eth1', params2={'ip': '10.0.2.1/24'})
        self.addLink(s3, s1, intfName2='s1-eth2', params2={'ip': '10.0.3.1/24'})

        self.addLink(s2, h3, intfName2='h3-eth0', params2={'ip': "10.0.2.5/24"})
        self.addLink(s2, h4, intfName2='h4-eth0', params2={'ip': "10.0.2.6/24"})
        self.addLink(s3, h5, intfName2='h5-eth0', params2={'ip': "10.0.3.5/24"})
        self.addLink(s3, h6, intfName2='h6-eth0', params2={'ip': "10.0.3.6/24"})
        # for h in hosts2:
        #     self.addLink(h, s2)
        # for h in hosts3:
        #     self.addLink(h, s3)

def run():
    topo = NetworkTopo()
    net = Mininet(topo=topo, controller=Controller, switch=OVSSwitch, waitConnected=True)

    net.start()

    info("*** Testing network\n")
    # net.pingAll()

    currnt_switch = "s2"
    # info(net[currnt_switch].cmd(f'ip address add 10.0.2.2/24 dev {currnt_switch}-eth1'))
    # info(net[currnt_switch].cmd(f'ip address add 10.0.2.3/24 dev {currnt_switch}-eth2'))
    # info(net[currnt_switch].cmd(f'ip address add 10.0.2.4/24 dev {currnt_switch}-eth3'))
    # info(net[currnt_switch].cmd(f'ovs-ofctl del-flows {currnt_switch}'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=1,arp,actions=flood'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.3.0/24,actions=output:1'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.2.5,actions=output:2'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.2.6,actions=output:3'))

    currnt_switch = "s1"
    # info(net[currnt_switch].cmd(f'ovs-ofctl del-flows {currnt_switch}'))
    info(net[currnt_switch].cmd(f'ovs-vsctl add-port {currnt_switch} {currnt_switch}-eth1'))
    info(net[currnt_switch].cmd(f'ovs-vsctl add-port {currnt_switch} {currnt_switch}-eth2'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=1,arp,actions=flood'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.2.0/24,actions=output:1'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.3.0/24,actions=output:2'))
    info(net[currnt_switch].cmd(f'ip link set ovs-system up'))

    currnt_switch = "s3"
    # info(net[currnt_switch].cmd(f'ip address add 10.0.3.2/24 dev {currnt_switch}-eth1'))
    # info(net[currnt_switch].cmd(f'ip address add 10.0.3.3/24 dev {currnt_switch}-eth2'))
    # info(net[currnt_switch].cmd(f'ip address add 10.0.3.4/24 dev {currnt_switch}-eth3'))
    # info(net[currnt_switch].cmd(f'ovs-ofctl del-flows {currnt_switch}'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=1,arp,actions=flood'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.2.0/24,actions=output:1'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.3.5,actions=output:2'))
    info(net[currnt_switch].cmd(f'ovs-ofctl add-flow {currnt_switch} ip,priority=10,nw_dst=10.0.3.6,actions=output:3'))

    info( "*** Running CLI\n" )
    CLI( net )

    info( "*** Stopping network\n" )
    net.stop()


if __name__ == '__main__':
    setLogLevel( 'info' )  # for CLI output
    run()